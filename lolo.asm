.MODEL SMALL	: Memberitahukan kepada  assembler bentuk memory yang digunakan oleh program.
.CODE	: Menyimpan program yang akan dijalankan. 
org 100h	: Memberitahukan assembler, program pada saat dijalankan(diload ke memory) ditaruh mulai pada offset ke 100h(256) byte. 
proses:			
jmp start	:  Melompat menuju tempat yang ditujukkan oleh perintah JUMP. 
oldX dw -1		
oldY dw 0		
start:	: Perintah memulai 
mov ah, 00	: text mode. 40x25. 16 colors. 8 pages.
mov al, 13h 	: reset disk system.
int 10h ;	: Melaksanakan program
mov ax, 0	: Memasukkan isi ax ke lokasi 0
int 33h	: Mendapatkan  lokasi mouse dan status button
cmp ax, 0	: Compare original number to zero
mov ax, 1	: Will set al (bottom 8 bits of ax) to 1
int 33h	: Mendapatkan lokasi mouse dan status button
check_mouse_button: 	: Periksa tombol mouse
mov ax, 3	: Will set al (bottom 8 bits of ax) to 3
int 33h	: Mendapatkan lokasi mouse dan status button
shr cx, 1 	: Untuk menggeser bit dari operan sebanyak 1 kali ke kanan
cmp bx, 1	: Membandingkan karakter
jne xor_cursor	
mov al, 1010b 	: Mendapatkan warna pixel
jmp draw_pixel	: Melompat ke bagian draw pixel
xor_cursor:	
cmp oldX, -1	
je not_required	
push cx	: Simpan nilai CX pada stack
push dx	: Simpan nilai DX pada stack
mov cx, oldX	
mov dx, oldY	
mov ah, 0dh 	: Fungsi ini menulis semua buffer disk yang telah dimodifikasi ke disk, tapi tidak memperbarui informasi direktori
int 10h	:Perintah melaksanakan program
xor al, 1111b 	: Warna Pixel
mov ah, 0ch 	: Flush buffer dan baca input standar
int 10h	: Perintah melaksanakan program
pop dx	: Mengeluarkan isi yang ada di stack dimulai dari data yang teratas general purpose register dx
pop cx	: Mengeluarkan isi yang ada di stack dimulai dari data yang teratas general purpose register cx
not_required:	: Tidak dibutuhkan
mov ah, 0dh 	: Fungsi ini menulis semua buffer disk yang dimodifikasi ke disk, tetapi tidak memperbarui informasi direktori
int 10h	: Perintah melaksanakan program
xor al, 1111b 	: Warna Pixel
mov oldX, cx	
mov oldY, dx	
draw_pixel:	: Perintah untuk menggambar pixel meliputi
mov ah, 0ch 	: Ubah warna untuk satu piksel
int 10h	: Perintah melaksanakan program
check_esc_key:	: Tombol esc untuk keluar
mov dl, 255	: Input parameter
mov ah, 6	: Scroll up window
int 21h	: Mencetak karakter
cmp al, 27 	: Membandingkan karakter
jne check_mouse_button	
stop:	: Perintah berhenti meliputi
;mov ax, 2 	: Menyembunyikan kursor mouse
;int 33h	: Inisialisasi mouse. setiap pointer mouse sebelumnya disembunyikan.
mov ax, 3 	: Kembali ke mode teks
int 10h	: Perintah melaksanakan program
; show box-shaped blinking text cursor:	  Tampilkan kursor teks berbentuk kotak berkedip
mov ah, 1	: Sembunyikan kursor teks yang berkedip
mov ch, 0	: Kursor start di baris 0
mov cl, 8	: Pembagi
int 10h	: Perintah melaksanakan program
mov dx, offset msg	: DX menunjuk ke (memegang alamat) string
mov ah, 9	: Mencetak sebuah string
int 21h	: Mencetak karakter
mov ah, 0	: Membaca dan menulis pixel dalam mode grafis
int 16h	: Berfungsi mengembalikan fungsi keyboard
ret	: Program berhenti
msg db " press any key.... $"	: Memuncukan kalimat saat di tekan tombol esc
end proses	: Proses akhir